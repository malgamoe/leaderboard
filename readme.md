# Leaderboard service

As a game developer, you need to create a dashboard that displays the high scores of each player in a scalable manner.
This dashboard will be visible to all players.
The game will send scores to the leaderboard through an API call to achieve this. The score combines the time taken to complete the game and the number of points earned.
The leaderboard will store this data and allow retrieval based on either time or number of points. 

- Which AWS services would you recommend to implement this system, and why?

  Lambda for the API since it is highly scalable, with dynamoDB to store the scores.
  The game can contact the lambda directly with the AWS sdk, otherwise in case other services exist or are planned it could use EventBridge to handle communication back and forth. Events would be stored on S3 in this case.
  The game has knowledge of the player using cognito or other service, so the leaderboard service should just receive the request from the game using as key an id of the player.

- Which API endpoint must be created to add scores and access the leaderboard by time or number of points?

  We can run an express app, with 2 endpoints:
  - the first to set the score `POST /score` with a json body with
    ```
    {
      userId: string,
      time: number,
      points: number
    }
    ```
  - the second to get the score `GET /score?orderBy=${time | points}`

- Lastly, what other API endpoint would you suggest adding, and why?

  First I would add that if you don't set orderBy in GET score, you get the global chart by score, the combination of time and points.

  Then I would add an other optional query argument to filter by player. One could see it's own scores, or even send a list of players. The game could allow players to have friends in the game, so you can see the leaderboard of a group of people easily.

  If we add a timestamp to the scores, the same endpoint could have filtering per period, last week, last month, since forever.

  We could also create an other enpoint `GET /score/stats` to get different stats about the scores, like average time/points of the players or any other stats like variance, etc...This could also have the periods filter.

- Explain how you will secure and deploy this service.

  The service is accessible only inside the infrastructure of AWS, we could add the API gateway with some specific rules but that seems overkill in this case.
  We can deploy the lambda with github actions to update the lambda on the merge of pull requests. The action compiles, build the code and generate a zip to deploy.
  We could also have a repo for the infratructure and using terraform with github action to manage deploys.

- Finally, please provide a pseudo-code or class diagram to represent the code architecture and organization of the service. Notably, it is unnecessary to implement the service.

  index.{js|ts} as the entrypoint, and defining the routing since we only have a few endpoints.

    ```
    imports ...

    define and setup express and logger

    // routes
    app.get('/score', (req, res) => scoreController.getScore(req, res))
    ... other endpoints
    
    ```

  For each route a file(in the most basic case we have only 1, for the `/score` route) with different controller functions for each of the actions.
    ```
    imports..

    export function getScoreController(req, res) {
      validate inputs in req, using zod or other

      call dynamoDB service (validated inputs)

      req.send(leaderboard)
    }

    ... other controller to set score

    ```

  A separate service file for accessing dynamoDB and having the controller not know about the implementation of storing and retrieving data from the DB.

    ```
    imports...
    
    define client and table

    export function getScore(ordering) {
      command({
        tablename,
        conditions
      })

      send command to client

      return response
    }

    ...other service functions

    ```

  We can add middleware for validation instead of doing it in controller, extract router in other file. Also create folders to organize files. But since the service is quite simple, I would do the changes if and when there is a need for it.

  